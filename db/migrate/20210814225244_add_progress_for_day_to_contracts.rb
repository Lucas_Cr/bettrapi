class AddProgressForDayToContracts < ActiveRecord::Migration[6.1]
  def change
    add_column :contracts, :progress_for_day, :integer
  end
end
