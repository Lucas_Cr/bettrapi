class AddNumOfPeriodsToBets < ActiveRecord::Migration[6.1]
  def change
    add_column :bets, :num_of_periods, :integer
  end
end
