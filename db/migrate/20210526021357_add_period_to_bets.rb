class AddPeriodToBets < ActiveRecord::Migration[6.1]
  def change
    add_column :bets, :time_of_day, :integer
    add_column :bets, :mulligan_period, :integer
  end
end
