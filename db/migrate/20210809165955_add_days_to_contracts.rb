class AddDaysToContracts < ActiveRecord::Migration[6.1]
  def change
    add_column :contracts, :sun, :integer
    add_column :contracts, :mon, :integer
    add_column :contracts, :tue, :integer
    add_column :contracts, :wed, :integer
    add_column :contracts, :thu, :integer
    add_column :contracts, :fri, :integer
    add_column :contracts, :sat, :integer
  end
end
