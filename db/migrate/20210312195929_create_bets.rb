class CreateBets < ActiveRecord::Migration[6.0]
  def change
    create_table :bets do |t|
      t.string :verb
      t.integer :more_or_less
      t.string :unit
      t.string :title
      t.integer :benchmark
      t.integer :mulligans
      t.integer :period
      t.integer :allowed_mulligans
      t.datetime :date_end
      t.datetime :date_start
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
    add_index :bets, [:user_id, :created_at]
  end
end
