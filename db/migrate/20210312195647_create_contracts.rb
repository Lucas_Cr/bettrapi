class CreateContracts < ActiveRecord::Migration[6.0]
  def change
    create_table :contracts do |t|
      t.integer :followed_bet_id
      t.integer :follower_id
      t.integer :progress
      t.integer :outcome
      t.integer :mulligan

      t.timestamps
    end
    add_index :contracts, :followed_bet_id
    add_index :contracts, :follower_id
    add_index :contracts, [:follower_id, :followed_bet_id], unique: true
  end
end
