class AddScheduleOffsetToContracts < ActiveRecord::Migration[6.1]
  def change
    add_column :contracts, :schedule_offset, :integer
  end
end
