class AddScheduleToContracts < ActiveRecord::Migration[6.1]
  def change
    add_column :contracts, :schedule, :string
  end
end
