module Api
  module V1
    class BetsController < Api::V1::ApiController
      helper_method :user
      # before_action :logged_in_user, only: [:create, :destroy]

      def show
        @bet = Bet.find(params[:id])
        render json: @bet
      end

      def index
        case (params[:sort])
          # must go back and convert all column names in the schema to lowercase in order for this function to work
        # when "popular"
        #   # puts("urgent")
        #   pagy, records = pagy(Bet.all.select('bets.*, contracts.*. COUNT(contracts.followed_bet_id) AS contract_count').joins(:contracts), items: 10)
        when "newest"
          # puts("newest")
          pagy, records = pagy(Bet.all.select('bets.*').where.not("bets.user_id" => current_user).where.missing(:contracts), items: 10)
        when "alpha"
          # puts("alpha")
          pagy, records = pagy(Bet.all.select('bets.*').where.not("bets.user_id" => current_user).where.missing(:contracts).reorder("bets.title ASC"), items: 10)
        # when "past"
        #   # puts("past")
        #   pagy, records = pagy(Bet.all.select('bets.*').where.not("bets.user_id" => current_user).reorder("time_of_day ASC"), items: 10)
        else
          pagy, records = pagy(Bet.all.select('bets.*').where.not("bets.user_id" => current_user).where.missing(:contracts), items: 10)
        end
        # pagy, records = pagy(Bet.all, items: 10)
        pagy_headers_merge(pagy)
        render json: records
        # render json: { data: @records,
        #        pagy: pagy_metadata(@pagy) }

        # @bets = Bet.find_each
        # render json: @bets
      end

      def manage_bets
        case (params[:sort])
          # must go back and convert all column names in the schema to lowercase in order for this function to work
        # when "popular"
        #   # puts("urgent")
        #   pagy, records = pagy(Bet.all.select('bets.*, contracts.*. COUNT(contracts.followed_bet_id) AS contract_count').joins(:contracts), items: 10)
        when "newest"
          # puts("newest")
          pagy, records = pagy(Bet.all.select('bets.*').where("bets.user_id" => current_user), items: 10)
        when "alpha"
          # puts("alpha")
          pagy, records = pagy(Bet.all.select('bets.*').where("bets.user_id" => current_user).reorder("bets.title ASC"), items: 10)
        # when "past"
        #   # puts("past")
        #   pagy, records = pagy(Bet.all.select('bets.*').where.not("bets.user_id" => current_user).reorder("time_of_day ASC"), items: 10)
        else
          pagy, records = pagy(Bet.all.select('bets.*').where("bets.user_id" => current_user), items: 10)
        end
        # pagy, records = pagy(Bet.all, items: 10)
        pagy_headers_merge(pagy)
        render json: records
        # render json: { data: @records,
        #        pagy: pagy_metadata(@pagy) }

        # @bets = Bet.find_each
        # render json: @bets
      end

      def followBet
        @bet = Bet.find(params[:id])
        # User.find(params[:id])
        puts("kkkkk")
        # puts(bet_params)
        puts(@bet.user_id)
        puts("BACK FOR BLOOD 2 - revenge of the puts statements")
        if current_user.followBet(@bet)
          # flash[:success] = "Bet created!"
          
          # redirect_to root_url
          render json: @bet
        else
          # render 'static_pages/home'
          render json: {
            error: "Error - didn't follow",
            status: 500
          }, status: 500
        end
      end

      def unfollowBet
        puts("snake? SNAKE? SNAAAAAAAAAAAKE???")
        @bet = Bet.find(params[:id])
        # User.find(params[:id])
        puts("ssssss ssssss")
        # puts(bet_params)
        puts(@bet.user_id)
        puts("Meet me in da club2")
        if current_user.unfollowBet(@bet)
          # flash[:success] = "Bet created!"
          
          # redirect_to root_url
          render json: @bet
        else
          # render 'static_pages/home'
          render json: {
            error: "Error - didn't unfollow",
            status: 500
          }, status: 500
        end
      end

      def checkIfFollowing
        @bet = Bet.find(params[:id])
        # User.find(params[:id])
        puts("ffffff")
        # puts(bet_params)
        puts(@bet.user_id)
        @res = false
        if current_user.followingBet?(@bet)
          @res = true   
          render json: @res
        else
          render json: @res
        end
      end

      def create
        puts("meet me in da club0")
        @bet = current_user.bets.build(bet_params)
        # 86400 == 24 x 60 x 60 == hours x minutes x seconds
        case @bet.period 
        when 1..7
          # a week
          @bet.num_of_periods = (@bet.date_end - @bet.date_start).to_i / (7.0 * 86400)
        when 8
          # biweekly
          @bet.num_of_periods = (@bet.date_end - @bet.date_start).to_i / (14.0 * 86400)
        when 9
          # monthly (4 weeks)
          @bet.num_of_periods = (@bet.date_end - @bet.date_start).to_i / (28.0 * 86400)
        end
        puts("meet me in da club1")
        puts(bet_params)
        puts(@bet.user_id)
        puts("Meet me in da club2")
        if @bet.save
          # flash[:success] = "Bet created!"
          current_user.followBet(@bet)
          # redirect_to root_url
          render json: @bet
        else
          # render 'static_pages/home'
          render json: {
            error: "Error - didn't save",
            status: 500
          }, status: 500
        end
      end
      
      def destroy
      end
      
      private
      
        def bet_params
          params.require(:bet).permit(
            :title,
            :verb,
            :benchmark,
            :more_or_less,
            :unit,
            :date_start,
            :date_end,
            :period,
            :time_of_day
          )
        end
    end
  end
end