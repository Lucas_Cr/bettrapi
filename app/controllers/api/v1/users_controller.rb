module Api
  module V1
    class UsersController < Api::V1::ApiController
      helper_method :user
      before_action :require_check_if_changing_password, only: :update

      def show; end

      def update
        user.update!(user_params)
      end
        
      def index
        pagy, records = pagy(User.all, items: 10)
        pagy_headers_merge(pagy)
        render json: records
      end

      # Follower functionality 

      def following
        @users = current_user.following
        render json: @users
      end
    
      def followers
        @users = current_user.followers
        render json: @users
      end

      def followUser
        @user = User.find(params[:id])
        if current_user.follow(@user)
          # render json: @user
          render json: {
            message: "followed",
            status: 200
          }, status: 200
        else
          render json: {
            error: "Error - didn't follow",
            status: 500
          }, status: 500
        end
      end

      def unfollowUser
        @user = User.find(params[:id])
        if current_user.unfollow(@user)
          # render json: @user
          render json: {
            message: "unfollowed",
            status: 200
          }, status: 200
        else
          render json: {
            error: "Error - didn't follow",
            status: 500
          }, status: 500
        end
      end

      def checkIfFollowing
        @user = User.find(params[:id])
        @res = false
        if current_user.following?(@user)
          @res = true   
          render json: @res
        else
          render json: @res
        end
      end

      # Bets functionality

      # def followingBets
      #   # puts("meet me in da club htsce")
      #   # puts(params[:user][:id])
      #   # puts("meet me in da club htsce")
      #   # @title = "Followed bets"
      #   @user  = User.find(params[:user][:id])

      #   # @bets = @user.followed_bets
      #   # puts("followingBets reached")
      #   # render json: @bets
      #   # @contracts = Contract.where(follower_id: current_user)
      #   puts("go go gadget fucking work already1")
      #   puts(params[:sort])
      #   puts("go go gadget fucking work already2")
      #   case (params[:sort])
      #   when "urgent"
      #     # puts("urgent")
      #     pagy, records = pagy(Bet.all.select('bets.*, contracts.*').joins(:contracts), items: 10)
      #     # pagy, records = pagy(@user.followed_bets.select('bets.*, contracts.*').joins(:contracts).where("contracts.follower_id = ?", @user.id).reorder('bets.time_of_day'), items: 10)
      #   when "newest"
      #     puts("newest")
      #     pagy, records = pagy(Bet.all.select('bets.*, contracts.*').joins(:contracts), items: 10)
      #   when "alpha"
      #     # puts("alpha")
      #     pagy, records = pagy(Bet.all.select('bets.*, contracts.*').joins(:contracts).reorder("bets.title ASC"), items: 10)
      #   when "past"
      #     # puts("past")
      #     pagy, records = pagy(Bet.all.select('bets.*, contracts.*').joins(:contracts).reorder("time_of_day ASC"), items: 10)
      #   else
      #     pagy, records = pagy(Bet.all.select('bets.*, contracts.*').joins(:contracts), items: 10)
      #   end
      #   pagy_headers_merge(pagy)
      #   render json: records
      # end

      def followed_bets
        @user  = User.find(params[:user][:id])

        case (params[:sort])
        when "urgent"
          puts("urgent")
          pagy, records = pagy(@user.followed_bets.select('contracts.*, bets.*').joins(:contracts).reorder('bets.time_of_day'), items: 10)
          # pagy, records = pagy(@user.followed_bets.select('bets.*, contracts.*').joins(:contracts).where("contracts.follower_id = ?", @user.id).reorder('bets.time_of_day'), items: 10)
        when "newest"
          puts("newest")
          pagy, records = pagy(@user.followed_bets.select('contracts.*, bets.*').joins(:contracts), items: 10)
        when "alpha"
          puts("alpha")
          pagy, records = pagy(@user.followed_bets.select('contracts.*, bets.*').joins(:contracts).reorder("bets.title ASC"), items: 10)
        when "past"
          puts("past")
          pagy, records = pagy(@user.followed_bets.select('contracts.*, bets.*').joins(:contracts).reorder("time_of_day ASC"), items: 10)
        else
          pagy, records = pagy(@user.followed_bets.select('contracts.*, bets.*').joins(:contracts), items: 10)
        end
        pagy_headers_merge(pagy)
        render json: records
      end

      private

      def user
        @user ||= current_user
      end

      def require_check_if_changing_password
        return if !user_params[:password] || user.valid_password?(params[:password_check])

        render_errors(I18n.t('errors.authentication.invalid_password_check'), :bad_request)
      end

      def user_params
        params.require(:user).permit(:email, :password, :first_name, :last_name, :locale)
      end
    end
  end
end
