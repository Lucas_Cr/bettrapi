module Api
  module V1
    class ContractsController < Api::V1::ApiController
        # before_action :logged_in_user
        def show
          @bet = Bet.find(params[:followed_bet_id])
          @contract = Contract.where(followed_bet_id: @bet.id, follower_id: current_user.id).first()
          render json: @contract
        end

        def create
          # add check to see if user already has contract with bet
          @contract = Contract.where(followed_bet_id: @bet.id, follower_id: current_user.id).first()
          if @contract
            render json: {
              error: "Error - already following",
              status: 500
            }, status: 500
          else
            @bet = Bet.find(params[:followed_bet_id])
            current_user.followBet(@bet)
            respond_to do |format|
              format.html { redirect_to @bet}
              format.js
            end
          end
        end

        def edit
          @bet = Bet.find(params[:followed_bet_id])
          @contract = Contract.where(followed_bet_id: @bet.id, follower_id: current_user.id).first()
          puts("edit contract 1")
          # puts(bet_params) this line fucks things up
          puts("@contract.progress: " + @contract.progress.to_s)
          puts("contract_params[:progress] " + contract_params[:progress].to_s )
          puts("contract_params[:progress_for_period] " + contract_params[:progress_for_period].to_s )

          puts("edit contact 2")
          # params[:progress] += @contract.progress
          # params[:contract][:mulligan] = contract_params[:mulligan].to_i + @contract.mulligan.to_i
          # params[:contract][:progress] = contract_params[:progress].to_i + @contract.progress.to_i
          # puts(contract_params[:progress] )

          @deadlinePassed
          manage_schedule()

          if (!@deadlinePassed) then update_progress() end
          

          if @contract.update(contract_params)
            # flash[:success] = "Profile updated"
            render json: @contract
            # redirect_to @user
          else
            render json: {
              error: "Error - didn't save",
              status: 500
            }, status: 500
          end
          # render file: "contracts/edit.html.erb"
          # render file: "contracts/edit.html.erb", locals: {bet: @bet, contract: @contract}
          # @contract = Contract.find(params[:followed_bet_id])
        end

        # def update
        #   @bet = Contract.find(params[:id]).followed_bet
        #   current_user.updateContract(@bet)
        #   respond_to do |format|
        #     format.html { redirect_to current_user }
        #     format.js
        #   end
        # end

        # Update this for api
        def destroy
          @bet = Contract.find(params[:id]).followed_bet
          current_user.unfollowBet(@bet)
          respond_to do |format|
            format.html { redirect_to @bet }
            format.js
          end
        end

        private

        def contract_params
          params.require(:contract).permit(
            :progress,
            :progress_for_day,
            :progress_for_period,
            :outcome,
            # :mulligan,
            :sun,
            :mon,
            :tue,
            :wed,
            :thu,
            :fri,
            :sat,
          )
        end

        def update_progress
          puts("Updating contract...")
          # @bet = Contract.find(params[:id]).followed_bet
          # params[:contract][:mulligan] = contract_params[:mulligan].to_i + @contract.mulligan.to_i
          
          # check to see if progress is complete for period
          @periodOutcome = false
          @flagDayAsDone = false
          @outcome = false
          params[:contract][:progress_for_day] = contract_params[:progress_for_day].to_i + @contract.progress_for_day.to_i
          case @bet. more_or_less.to_i
          when -1
            # "equal or less"
            if (contract_params[:progress_for_day].to_i <= @bet.benchmark) then @flagDayAsDone = true end
          when 0
            # "equal"
            if (contract_params[:progress_for_day].to_i == @bet.benchmark) then @flagDayAsDone = true end
          when 1
            # "equal or more"
            if (contract_params[:progress_for_day].to_i >= @bet.benchmark) then @flagDayAsDone = true end
              puts("xxxxxxx flagDayAsDone: ")
              puts(@flagDayAsDone)
          end
          if (!@flagDayAsDone)
            puts("progress for day incomplete, return")
            # progress for day incomplete, update contract_params and return
            
            return
          end
          puts("if reached progress for day complete,continue")
          # if reached progress for day complete, update contract_params and continue
          # params[:contract][:progressForDay] = contract_params[:progressForDay].to_i + @contract.progressForDay.to_i

          if (@flagDayAsDone)
            case Date.current.wday
            when 0
              params[:contract][:sun] = 1
            when 1
              params[:contract][:mon] = 1
            when 2
              params[:contract][:tue] = 1
            when 3
              params[:contract][:wed] = 1
            when 4
              params[:contract][:thu] = 1
            when 5
              params[:contract][:fri] = 1
            when 6
              params[:contract][:sat] = 1
            end
            params[:contract][:progress_for_period] = @bet.benchmark.to_i + @contract.progress_for_period.to_i
            # reset progress for the day
            params[:contract][:progress_for_day] = 0
            case @bet. more_or_less.to_i
            when -1
              # "equal or less"
              if (contract_params[:progress_for_Period].to_i <= @bet.benchmark * @bet.period) then @periodOutcome = true end
            when 0
              # "equal"
              if (contract_params[:progress_for_Period].to_i == @bet.benchmark * @bet.period) then @periodOutcome = true end
            when 1
              # "equal or more"
              if (contract_params[:progress_for_Period].to_i >= @bet.benchmark * @bet.period) then @periodOutcome = true end
                puts("xxxxxxx period for progress is true: ")
                puts(@periodOutcome)
            end
            if (!@periodOutcome)
              puts("progress for period incomplete, return")
              # progress for day incomplete, update contract_params and return
              
              return
            end
            puts("if od complete, update contract_params and continue")
            # if reached progress for period complete, update contract_params and continue
            # params[:contract][:progressForPeriod] = contract_params[:progressForPeriod].to_i + @contract.progressForPeriod.to_i

          end
          # check to see if total progress is complete
          if (@periodOutcome)
            # add period progress to total progress and compare against benchmark * total periods 
            # remove contract_params[:progress].to_i from this line as it was only for testing
            params[:contract][:progress] = contract_params[:progress].to_i + contract_params[:progress_for_period].to_i  + @contract.progress_for_period.to_i
            # reset progress for the period
            params[:contract][:progress_for_period] = 0
            case @bet. more_or_less.to_i
            when -1
              # "equal or less"
              if (contract_params[:progress].to_i <= (@bet.benchmark * @bet.num_of_periods * @bet.period)) then @outcome = true end
            when 0
              # "equal"
              if (contract_params[:progress].to_i == (@bet.benchmark * @bet.num_of_periods * @bet.period)) then @outcome = true end
            when 1
              # "equal or more"
              if (contract_params[:progress].to_i >= (@bet.benchmark * @bet.num_of_periods * @bet.period)) then @outcome = true end
            else
              puts("# progress incomplete, return")
              # progress incomplete, return
              return
            end
            if (@outcome)
              puts("# bet complete, said outcome to 1, return")
              params[:contract][:outcome] = 1 
              return
            end
          end
        end

        def manage_schedule 
          params[:contract][:sun] = @contract.sun
          params[:contract][:mon] = @contract.mon
          params[:contract][:tue] = @contract.tue
          params[:contract][:wed] = @contract.wed
          params[:contract][:thu] = @contract.thu
          params[:contract][:fri] = @contract.fri
          params[:contract][:sat] = @contract.sat
          # if day is already completed || bet has been failed then return (move to update_progress)
          @daysCompleted = 0
          @curHour = '%02d' % Time.now.hour
          @curMin  = '%02d' % Time.now.min

          puts("currentTime: " + Time.now.strftime('%m/%d/%Y %H:%M %p'))
          puts("current Hour: " + @curHour)
          puts("current min: " + @curMin)
          puts("endOfWeek: " + DateTime.current.end_of_week.strftime('%m/%d/%Y %H:%M %p'))
          @daysLeft = ((DateTime.current.at_end_of_week.to_i - DateTime.current.to_i)/ ( 60 * 60 * 24))
          @daysSinceUpdate = ((DateTime.current.to_i - @contract.updated_at.to_i)/ ( 60 * 60 * 24))
          puts("daysLeft: " + @daysLeft.to_s)
          puts("daysSinceUpdate: " + @daysSinceUpdate.to_s)

          # if period has passed then outcome = 3 (ie bet abandoned)
          if (@bet.period.to_i < @daysSinceUpdate)
            puts("bet abandoned - set outcome to 3")
            params[:contract][:outcome] = 3
            @contract.outcome = 3
            return
          end
          puts("Bet still active proceed to manage schedule")
          
          puts("deadline Hour: " + @bet.time_of_day.to_s.first(2).to_s)
          puts("deadline min: " + @bet.time_of_day.to_s.last(2).to_s)
          # if passed deadline for day
          if ((@curHour.to_i == @bet.time_of_day.to_s.first(2).to_i && @curMin.to_i > @bet.time_of_day.to_s.last(2).to_i) || @curHour.to_i > @bet.time_of_day.to_s.first(2).to_i)
            # @num = (@contract.mulligan > 0) ? 3 : -1
            @num = -1
            # puts("mulligans is " + @contract.mulligan.to_s)
            # if (@num == 3) then @contract.mulligan-=1 end
            # puts("mulligans should be one less if num is 3 " + @contract.mulligan.to_s)
            puts("num is " + @num.to_s)
            puts("Day of week is " + Date.current.wday.to_s)
            @deadlinePassed = (@num == -1) ? true : false
            case Date.current.wday
            when 0
              params[:contract][:sun], @contract.sun = @num
            when 1
              params[:contract][:mon], @contract.mon = @num
            when 2
              params[:contract][:tue], @contract.tue = @num
            when 3
              params[:contract][:wed], @contract.wed = @num
            when 4
              params[:contract][:thu], @contract.thu = @num
            when 5
              params[:contract][:fri], @contract.fri = @num
            when 6
              params[:contract][:sat], @contract.sat = @num
            end
          end

          # if not passed deadline add 1 to days left (because they could still complete the bet for today)
          if !@deadlinePassed then @daysLeft+=1 end
          puts("daysLeft adjusted: " + @daysLeft.to_s)

          # # totals number of day completed for later calculations
          if @contract.sun == 1 || @contract.sun == 3 then @daysCompleted+=1 end
          if @contract.mon == 1 || @contract.mon == 3 then @daysCompleted+=1 end
          if @contract.tue == 1 || @contract.tue == 3 then @daysCompleted+=1 end
          if @contract.wed == 1 || @contract.wed == 3 then @daysCompleted+=1 end
          if @contract.thu == 1 || @contract.thu == 3 then @daysCompleted+=1 end
          if @contract.fri == 1 || @contract.fri == 3 then @daysCompleted+=1 end
          if @contract.sat == 1 || @contract.sat == 3 then @daysCompleted+=1 end
          # # repeat for week
          puts("daysCompleted: " + @daysCompleted.to_s)

          # this if statement determines the user necassarily failed at least 1 day this week prior
          if @daysCompleted + @daysLeft < @bet.period
            puts("@daysCompleted + @daysLeft < @bet.period no way to rearrange days to avoid failure")
            @daysFailed =  @bet.period - (@daysCompleted + @daysLeft)
            if (@daysFailed > Date.current.wday) then @daysFailed = Date.current.wday end
            # @daysFailed = (@daysFailed > Date.current.wday) ? Date.current.wday : @bet.period - (@daysCompleted + @daysLeft)
            puts("days failed: " + @daysFailed.to_s)
            if (@daysFailed > Date.current.wday) then daysFailed =Date.current.wday end

            @index = Date.current.wday
            # if we aren't passed deadline don't treat it as foregone
            if !@deadlinePassed then @index-=1 end

            while (@index >= 0)
              # if we've reached a day that has previously been accounted for break from update as this line of code is only designed
              # to account for the days missed since last update
              puts(" # if this day has already been allocated a value then break, if negative count against days failed")
              @index-=1
              case (@index + 1)
              when 0
                if (@contract.sun == 1) || (@contract.sun == 3)
                  puts("found passed day: " + (@index + 1).to_s)
                  next 
                end
                if (@contract.sun == -1)
                  @daysFailed-=1
                  puts("found failed day: " + (@index + 1).to_s)
                    next
                end
              when 1
                if (@contract.mon == 1) || (@contract.mon == 3) then next end
                if (@contract.mon == -1)
                  @daysFailed-=1 
                  puts("found failed day: " + (@index + 1).to_s)
                  next
                end
              when 2
                if (@contract.tue == 1) || (@contract.tue == 3)
                  puts("found passed day: " + (@index + 1).to_s)
                  next 
                end
                if (@contract.tue == -1)
                  @daysFailed-=1
                  puts("found failed day: " + (@index + 1).to_s)
                    next
                end
              when 3
                if (@contract.wed == 1) || (@contract.wed == 3)
                  puts("found passed day: " + (@index + 1).to_s)
                  next 
                end
                if (@contract.wed == -1)
                  @daysFailed-=1
                  puts("found failed day: " + (@index + 1).to_s)
                  next
                end
              when 4
                if (@contract.thu == 1) || (@contract.thu == 3)
                  puts("found passed day: " + (@index + 1).to_s)
                  next 
                end
                if (@contract.thu == -1)
                  @daysFailed-=1
                  puts("found failed day: " + (@index + 1).to_s)
                  next
                end
              when 5
                if (@contract.fri == 1) || (@contract.fri == 3)
                  puts("found passed day: " + (@index + 1).to_s)
                  next 
                end
                if (@contract.fri == -1)
                  @daysFailed-=1
                  puts("found failed day: " + (@index + 1).to_s)
                  next
                end
              when 6
                if (@contract.sat == 1) || (@contract.sat == 3)
                  puts("found passed day: " + (@index + 1).to_s)
                  next 
                end
                if (@contract.sat == -1)
                  @daysFailed-=1
                  puts("found failed day: " + (@index + 1).to_s)
                  next
                end
              end
                # if here the previous day was unaccounted for 
              puts("# if here the given day was unaccounted for")
              # if (@contract.mulligan > 0)
              #   # set day to 3
              #   @num = 3
              #   @contract.mulligan-=1
              #   @daysFailed-=1
              # els
              if (@daysFailed > 0)
                puts("adjusting day: " + (@index + 1).to_s )
                #set day to -1
                @num = -1
                @daysFailed-=1
              
                case (@index + 1)
                when 0
                  params[:contract][:sun], @contract.sun = @num
                when 1
                  params[:contract][:mon], @contract.mon = @num
                when 2
                  params[:contract][:tue], @contract.tue = @num
                when 3
                  params[:contract][:wed], @contract.wed = @num
                when 4
                  params[:contract][:thu], @contract.thu = @num
                when 5
                  params[:contract][:fri], @contract.fri = @num
                when 6
                  params[:contract][:sat], @contract.sat = @num
                end
              end
            end
            puts("@contract: ")
            pp(@contract)
            puts("@bet")
            pp(@bet)

          end
        end


    end
  end
end
