module Api
  module V1
    class RelationshipsController < Api::V1::ApiController
        before_action :logged_in_user
      
        def create
          @user = User.find(params[:followed_id])
          render json: @user
        end
      
        def destroy
          @user = Relationship.find(params[:id]).followed
          current_user.unfollow(@user)
          render json: @user
        end
      end
  end
end