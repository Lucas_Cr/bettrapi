# == Schema Information
#
# Table name: bets
#
#  id                :bigint           not null, primary key
#  allowed_mulligans :integer
#  benchmark         :integer
#  date_end          :datetime
#  date_start        :datetime
#  more_or_less      :integer
#  mulligan_period   :integer
#  mulligans         :integer
#  num_of_periods    :integer
#  period            :integer
#  time_of_day       :integer
#  title             :string
#  unit              :string
#  verb              :string
#  created_at        :datetime         not null, indexed => [user_id]
#  updated_at        :datetime         not null
#  user_id           :bigint           not null, indexed, indexed => [created_at]
#
# Indexes
#
#  index_bets_on_user_id                 (user_id)
#  index_bets_on_user_id_and_created_at  (user_id,created_at)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Bet < ApplicationRecord
    belongs_to :user
    # has_many :contracts
    # Bet stuff
    has_many :contracts, class_name:  "Contract",
    foreign_key: "followed_bet_id",
    dependent:   :destroy
    has_many :followers, through: :contracts, source: :follower
  
    default_scope -> { order(created_at: :desc) }
    validates :user_id, presence: true
    validates :title, presence: true, length: { maximum: 140 }
    validates :verb, presence: true, length: { maximum: 140 }
    validates :benchmark, presence: true, numericality: { greater_than_or_equal_to: -1000, less_than_or_equal_to: 1000 }
    validates :num_of_periods, numericality: { greater_than_or_equal_to: -10000, less_than_or_equal_to: 10000 }
    validates :time_of_day, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 2359 }
    validates :more_or_less, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }
    validates :unit, presence: true, length: { maximum: 140 }
    validates :date_start, presence: true, date: true
    validates :date_end, presence: true, date: { after: :date_start }
end
