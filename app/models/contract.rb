# == Schema Information
#
# Table name: contracts
#
#  id                  :bigint           not null, primary key
#  fri                 :integer
#  mon                 :integer
#  mulligan            :integer
#  outcome             :integer
#  progress            :integer
#  progress_for_day    :integer
#  progress_for_period :integer
#  sat                 :integer
#  schedule            :string
#  schedule_offset     :integer
#  sun                 :integer
#  thu                 :integer
#  tue                 :integer
#  wed                 :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  followed_bet_id     :integer          indexed, indexed => [follower_id]
#  follower_id         :integer          indexed, indexed => [followed_bet_id]
#
# Indexes
#
#  index_contracts_on_followed_bet_id                  (followed_bet_id)
#  index_contracts_on_follower_id                      (follower_id)
#  index_contracts_on_follower_id_and_followed_bet_id  (follower_id,followed_bet_id) UNIQUE
#
class Contract < ApplicationRecord
    belongs_to :follower, class_name: "User"
    belongs_to :followed_bet, class_name: "Bet"
    validates :follower_id, presence: true
    validates :followed_bet_id, presence: true

    default_scope -> { order(created_at: :desc) }
    # validates :progress_for_day, numericality: { greater_than_or_equal_to: -1000, less_than_or_equal_to: 1000 }
    # validates :progress, numericality: { greater_than_or_equal_to: -100000, less_than_or_equal_to: 100000 }
    # validates :outcome, numericality: { greater_than_or_equal_to: -1000, less_than_or_equal_to: 1000 }
    # validates :sun, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }
    # validates :tue, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }
    # validates :mon, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }
    # validates :wed, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }
    # validates :thu, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }
    # validates :fri, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }
    # validates :sat, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }


    def activateContract(progress)
        update_attribute(:progress,    progress)
        update_attribute(:progress_for_day,    0)
        update_attribute(:sun,    0)
        update_attribute(:mon,    0)
        update_attribute(:tue,    0)
        update_attribute(:wed,    0)
        update_attribute(:thu,    0)
        update_attribute(:fri,    0)
        update_attribute(:sat,    0)
        update_attribute(:outcome, 0)
    end
end
