# == Schema Information
#
# Table name: contracts
#
#  id                  :bigint           not null, primary key
#  fri                 :integer
#  mon                 :integer
#  mulligan            :integer
#  outcome             :integer
#  progress            :integer
#  progress_for_day    :integer
#  progress_for_period :integer
#  sat                 :integer
#  schedule            :string
#  schedule_offset     :integer
#  sun                 :integer
#  thu                 :integer
#  tue                 :integer
#  wed                 :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  followed_bet_id     :integer          indexed, indexed => [follower_id]
#  follower_id         :integer          indexed, indexed => [followed_bet_id]
#
# Indexes
#
#  index_contracts_on_followed_bet_id                  (followed_bet_id)
#  index_contracts_on_follower_id                      (follower_id)
#  index_contracts_on_follower_id_and_followed_bet_id  (follower_id,followed_bet_id) UNIQUE
#
FactoryBot.define do
  factory :contract do
    followBet_id { 1 }
    follower_id { 1 }
    progress { 1 }
    outcome { 1 }
    mulligan { 1 }
  end
end
