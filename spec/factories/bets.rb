# == Schema Information
#
# Table name: bets
#
#  id                :bigint           not null, primary key
#  allowed_mulligans :integer
#  benchmark         :integer
#  date_end          :datetime
#  date_start        :datetime
#  more_or_less      :integer
#  mulligan_period   :integer
#  mulligans         :integer
#  num_of_periods    :integer
#  period            :integer
#  time_of_day       :integer
#  title             :string
#  unit              :string
#  verb              :string
#  created_at        :datetime         not null, indexed => [user_id]
#  updated_at        :datetime         not null
#  user_id           :bigint           not null, indexed, indexed => [created_at]
#
# Indexes
#
#  index_bets_on_user_id                 (user_id)
#  index_bets_on_user_id_and_created_at  (user_id,created_at)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :bet do
    
  end
end
