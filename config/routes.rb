require 'sidekiq/web'

Rails.application.routes.draw do

  post 'api/v1/users/index', to: 'api/v1/users#index'
  # Relationship routes
  post 'api/v1/following', to: 'api/v1/users#following'
  post 'api/v1/followers', to: 'api/v1/users#followers'
  post 'api/v1/checkIfFollowingUser', to: 'api/v1/users#checkIfFollowing'
  post 'api/v1/followUser', to: 'api/v1/users#followUser'
  post 'api/v1/unfollowUser', to: 'api/v1/users#unfollowUser'

  # Bet routes
  post 'api/v1/bets/index', to: 'api/v1/bets#index'
  post 'api/v1/bets/manage_bets', to: 'api/v1/bets#manage_bets'
  post 'api/v1/users/followed_bets', to: 'api/v1/users#followed_bets'
  post 'api/v1/checkIfFollowingBet', to: 'api/v1/bets#checkIfFollowing'
  post 'api/v1/followBet', to: 'api/v1/bets#followBet'
  post 'api/v1/unfollowBet', to: 'api/v1/bets#unfollowBet'
  post 'api/v1/contracts/edit', to: 'api/v1/contracts#edit'
  post 'api/v1/contracts/show', to: 'api/v1/contracts#show'

  mount Sidekiq::Web => '/jobmonitor'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  mount_devise_token_auth_for 'User', at: '/api/v1/users', controllers: {
    registrations: 'api/v1/registrations',
    sessions: 'api/v1/sessions',
    passwords: 'api/v1/passwords',
    token_validations: 'api/v1/token_validations'
  }, skip: %i[omniauth_callbacks registrations]

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resource :bets

      resource :user, only: %i[show update]

      devise_scope :user do
        resources :users, only: [] do
          controller :registrations do
            post :following, :followers, :create, on: :collection
          end
        end
      end

      
    end
  end
end
